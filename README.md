# Experiment WebRTC

> Experiment from 2014

This is a small project that intends to test WebRTC.

It is a simple page with a button that enables to take a picture from the
computer webcam and display it on the page.

You can check out my article to know more about [why I document my process when
I'm testing something new for
me](https://code.likeagirl.io/the-art-of-documenting-your-coding-experience-e868e28cdc45).

## Requirements

**You need to host this app on a secure domain with `https` in order to use
`navigator.getUserMedia`**.
